﻿using Formularios.Movil.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Formularios.Movil.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FormPage : ContentPage
    {
        public FormPage(Type tipoModelo)
        {
            InitializeComponent();
            this.BindingContext = new FormViewModel(tipoModelo);
        }
    }
}