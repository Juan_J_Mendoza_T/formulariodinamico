﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Formularios.Movil.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ControlAttribute : Attribute
    {
        private bool _isVisible;
        private string _display;
        private Type _controlType;
        private string _nombrePropiedad;
        public ControlAttribute(bool isVisible, string display)
        {
            _isVisible = isVisible;
            _display = display;
        }

        public virtual string Display => _display;

        public virtual bool IsVisible => _isVisible;

        public virtual Type ControlType
        {
            get => _controlType;
            set => _controlType = value;
        }

        public virtual string NombrePropiedad
        {
            get => _nombrePropiedad;
            set => _nombrePropiedad = value;
        }
    }
}
