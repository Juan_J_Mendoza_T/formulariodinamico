﻿using Formularios.Movil.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Formularios.Movil.Models
{
    public class LugarModel
    {
        [ControlAttribute(true, "Nombre de lugar", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public string Nombre { get; set; }
        [ControlAttribute(true, "Latitud", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public float Latitud { get; set; }
        [ControlAttribute(true, "Longitud", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public float Longitud { get; set; }
        [ControlAttribute(true, "Descripción", ControlType = typeof(Editor), NombrePropiedad = "TextProperty")]
        public string Descripcion { get; set; }
    }
}
