﻿using Formularios.Movil.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Formularios.Movil.Models
{
    public class LibroModel
    {
        [ControlAttribute(false, "Id")]
        public string Id { get; set; }
        [ControlAttribute(true, "Titulo", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public string Titulo { get; set; }
        [ControlAttribute(true, "Nombre de autor", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public string Autor { get; set; }
        [ControlAttribute(true, "Fecha de publicación", ControlType = typeof(DatePicker), NombrePropiedad = "DateProperty")]
        public DateTime FechaPublicacion { get; set; }
    }
}
