﻿using Formularios.Movil.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Formularios.Movil.Models
{
    public class PersonaModel
    {
        [ControlAttribute(true, "Nombre (s)", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public string Nombre { get; set; }
        [ControlAttribute(true, "Primer Apellido", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public string PrimerApellido { get; set; }
        [ControlAttribute(true, "Segundo Apellido", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public string SegundoApellido { get; set; }
        [ControlAttribute(true, "Edad", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public int Edad { get; set; }
        [ControlAttribute(true, "Pais", ControlType = typeof(Entry), NombrePropiedad = "TextProperty")]
        public string Pais { get; set; }
    }
}
