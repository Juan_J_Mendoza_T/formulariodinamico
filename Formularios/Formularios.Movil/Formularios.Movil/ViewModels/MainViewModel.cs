﻿using Formularios.Movil.Models;
using Formularios.Movil.Pages;
using System;
using Xamarin.Forms;

namespace Formularios.Movil.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public MainViewModel()
        {
            IrAFormCommand = new Command(IrAFormCommandExecute);
        }

        public Command IrAFormCommand { get; set; }

        private async void IrAFormCommandExecute(object obj)
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            switch (int.Parse(obj.ToString()))
            {
                case 1:
                    await App.Current.MainPage.Navigation.PushModalAsync(new FormPage(typeof(LibroModel)));
                    break;
                case 2:
                    await App.Current.MainPage.Navigation.PushModalAsync(new FormPage(typeof(PersonaModel)));
                    break;
                case 3:
                    await App.Current.MainPage.Navigation.PushModalAsync(new FormPage(typeof(LugarModel)));
                    break;
            }
            IsBusy = false;
        }
    }
}
