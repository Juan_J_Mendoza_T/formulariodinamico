﻿using Formularios.Movil.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Xamarin.Forms;

namespace Formularios.Movil.ViewModels
{
    public class FormViewModel : BaseViewModel
    {
        private object _modelo;

        public object Modelo
        {
            get { return _modelo; }
            set { SetProperty(ref _modelo, value); }
        }

        private StackLayout _contenedor = new StackLayout();

        public StackLayout Contenedor
        {
            get { return _contenedor; }
            set { SetProperty(ref _contenedor, value); }
        }

        public FormViewModel(Type tipoModelo)
        {
            GuardarCommand = new Command(GuardarCommandExecute);
            Modelo = Activator.CreateInstance(tipoModelo);
            Contenedor = GenerarCampos(tipoModelo);
        }

        private StackLayout GenerarCampos(Type tipoModelo)
        {
            StackLayout campos = new StackLayout() { VerticalOptions = LayoutOptions.FillAndExpand, HorizontalOptions = LayoutOptions.FillAndExpand };
            foreach (PropertyInfo propiedad in tipoModelo.GetProperties())
            {
                if (propiedad.CustomAttributes.Where(atributo => atributo.AttributeType.Name == nameof(ControlAttribute)).FirstOrDefault() is CustomAttributeData atributoDePropiedad)
                {
                    if ((bool)atributoDePropiedad.ConstructorArguments[0].Value)
                    {
                        if (Activator.CreateInstance((Type)atributoDePropiedad.NamedArguments.Where(argumento => argumento.MemberName == "ControlType").FirstOrDefault().TypedValue.Value) is View element)
                        {
                            campos.Children.Add(new Label { Text = (string)atributoDePropiedad.ConstructorArguments[1].Value });
                            FieldInfo propiedadBindable = element.GetType().GetField((string)atributoDePropiedad.NamedArguments.Where(argumento => argumento.MemberName == "NombrePropiedad").FirstOrDefault().TypedValue.Value);
                            element.SetBinding((BindableProperty)propiedadBindable.GetValue(element), new Binding($"Modelo.{propiedad.Name}") { Source = this });
                            campos.Children.Add(element);
                        }
                    }
                }
            }
            return campos;
        }

        public Command GuardarCommand { get; set; }

        private async void GuardarCommandExecute(object obj)
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            string text = "";
            PropertyInfo[] propiedades = Modelo.GetType().GetProperties();
            for (int i = 0; i < propiedades.Length; i++)
            {
                text += $"{propiedades[i].Name}: {propiedades[i].GetValue(Modelo)}";
                if (i < (propiedades.Length - 1))
                {
                    text += "\n";
                }
            }
            await App.Current.MainPage.DisplayAlert("Los valores enlazados son:", text, "Ok");
            await App.Current.MainPage.Navigation.PopModalAsync();
            IsBusy = false;
        }
    }
}
